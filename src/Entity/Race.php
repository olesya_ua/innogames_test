<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RaceRepository")
 * @ORM\Table(name="race")
 * @HasLifecycleCallbacks
 */
class Race
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $startedAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $finishedAt;

    /**
     * @var ArrayCollection|Horse[]
     * @ORM\OneToMany(targetEntity="App\Entity\Horse", mappedBy="race", cascade={"persist"})
     * @ORM\OrderBy({"currentDistance" = "ASC"})
     */
    private $horses;

    public function __construct()
    {
        $this->horses = new ArrayCollection();
        $this->startedAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getStartedAt(): \DateTime
    {
        return $this->startedAt;
    }

    /**
     * @return \DateTime
     */
    public function getFinishedAt(): \DateTime
    {
        return $this->finishedAt;
    }

    public function getHorses(): Collection
    {
        return $this->horses;
    }

    public function setFinishedAt(): void
    {
        $this->finishedAt = new \DateTime();
    }

    public function addHorse(Horse $horse): void
    {
        $horse->setRace($this);
        $this->horses->add($horse);
    }

    public function getTopHorses(): array
    {
        $horses = $this->getHorses()->toArray();
        uasort($horses, function(Horse $a, Horse $b) {
            $finishedInSecondsA = $a->getFinishedInSeconds();
            $finishedInSecondsB = $b->getFinishedInSeconds();

            if ($finishedInSecondsA == $finishedInSecondsB) {
                return 0;
            }
            return ($finishedInSecondsA < $finishedInSecondsB) ? -1 : 1;
        });

        return $horses;
    }

    public function getRaceTime(): int
    {
        $horseTime = [];

        foreach ($this->getHorses() as $horse) {
            $horseTime[] = $horse->getFinishedInSeconds() ?: 0;
        }

        return max($horseTime);
    }

    public function advance(int $totalDistance, int $seconds): void
    {
        $isRaceFinished = true;

        foreach ($this->horses as $horse) {
            $horse->move($seconds, $totalDistance);
            if ($horse->getCurrentDistance() < $totalDistance) {
                $isRaceFinished = false;
            }
        }


        if ($isRaceFinished) {
            $this->setFinishedAt();
        }
    }
}
