<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HorseRepository")
 * @ORM\Table(name="horse")
 */
class Horse
{
    const BASE_SPEED = 5;

    const JOCKEY_SLOWS_SPEED = 5;

    const STRENGTH_PERCENT = 8;

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $speed;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $strength;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $endurance;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $finishedInSeconds;

    /**
     * @var Race
     * @ORM\ManyToOne(targetEntity="App\Entity\Race", inversedBy="horses", cascade={"persist"})
     */
    private $race;

    /**
     * @var float
     * @ORM\Column(type="float", options={"unsigned":true, "default":0})
     */
    private $currentDistance = 0;

    public function __construct(float $speed, float $strength, float $endurance)
    {
        $this->speed = $speed;
        $this->strength = $strength;
        $this->endurance = $endurance;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCurrentDistance(): float
    {
        return $this->currentDistance;
    }

    public function getSpeed(): float
    {
        return $this->speed;
    }

    public function getStrength(): float
    {
        return $this->strength;
    }

    public function getEndurance(): float
    {
        return $this->endurance;
    }

    public function getFinishedInSeconds(): ?int
    {
        return $this->finishedInSeconds;
    }

    public function setCurrentDistance(float $currentDistance): void
    {
        $this->currentDistance = $currentDistance;
    }

    public function setRace(Race $race): void
    {
        $this->race = $race;
    }

    public function setFinishedInSeconds(int $finishedInSeconds): void
    {
        $this->finishedInSeconds = $finishedInSeconds;
    }

    public function move(int $seconds, int $distanceLimit)
    {
        $distanceToChangeSpeedAt = $this->getEndurance()*100;
        $prevDistance = $this->getCurrentDistance();

        if ($prevDistance < $distanceToChangeSpeedAt) {
            $horseSpeed = $this->getCalculatedSpeed(false);
        } else {
            $horseSpeed = $this->getCalculatedSpeed();
        }

        $targetDistance = $prevDistance + $horseSpeed*$seconds;

        if ($prevDistance < $distanceToChangeSpeedAt && $targetDistance > $distanceToChangeSpeedAt) {
            $distanceWithoutSlow = $distanceToChangeSpeedAt - $prevDistance;
            $secondsToRunWithoutSlow = $distanceWithoutSlow / $horseSpeed;

            $secondsLeft = $seconds - $secondsToRunWithoutSlow;
            $distanceWithSlow = $this->getCalculatedSpeed() * $secondsLeft;

            $targetDistance = $prevDistance + $distanceWithoutSlow + $distanceWithSlow;
        }

        if ($targetDistance > $distanceLimit) {
            $overflownDistance = $targetDistance - $distanceLimit;
            $seconds -= $overflownDistance / $horseSpeed;
            $targetDistance = $distanceLimit;
        }

        $this->setCurrentDistance($targetDistance);
        $this->setFinishedInSeconds(
            $this->getFinishedInSeconds() + $seconds
        );
    }

    private function getCalculatedSpeed(bool $withJockeySlow = true): float
    {
        $jockeySlowsHorse = 0;

        if ($withJockeySlow) {
            $jockeySlowsHorsePercent = $this->strength * self::STRENGTH_PERCENT;
            $jockeySlowsHorse = self::JOCKEY_SLOWS_SPEED * (1 - $jockeySlowsHorsePercent/100);
        }

        return $this->speed + self::BASE_SPEED - $jockeySlowsHorse;
    }
}
