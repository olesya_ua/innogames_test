<?php

namespace App\Controller;

use App\Entity\Race;
use App\Factory\RaceFactory;
use App\Repository\HorseRepository;
use App\Repository\RaceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RaceController extends AbstractController
{
    /**
     * @param RaceRepository $raceRepository
     * @param HorseRepository $horseRepository
     *
     * @return Response
     *
     * @Route("/", name="index")
     */
    public function indexAction(RaceRepository $raceRepository, HorseRepository $horseRepository)
    {
        return $this->render('innogames/race.html.twig', [
            'currentRaces' => $raceRepository->getCurrentRaces(),
            'lastRaces' => $raceRepository->getLastRaces(),
            'bestHorse' => $horseRepository->findBestHorse(),
        ]);
    }

    /**
     * @param RaceFactory $raceFactory
     *
     * @return Response
     *
     * @Route("/create", methods={"POST"}, name="create-race")
     */
    public function createAction(RaceFactory $raceFactory)
    {
        try {
            $raceFactory->createRace();
        } catch (\Exception $exception) {
            $this->addFlash('error', $exception->getMessage());
        }

        return $this->redirectToRoute('index', [], 302);
    }

    /**
     * @param int $totalDistance
     * @param int $raceAdvanceTime
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     *
     * @Route("/run", methods={"POST"}, name="run-race")
     */
    public function runAction(
        int $totalDistance,
        int $raceAdvanceTime,
        EntityManagerInterface $entityManager
    ) {
        try {
            /** @var RaceRepository $repository */
            $repository = $entityManager->getRepository(Race::class);
            $races = $repository->findBy(
                ['finishedAt' => null]
            );

            foreach ($races as $race) {
                $race->advance($totalDistance, $raceAdvanceTime);
            }
            $entityManager->flush();
        } catch (\Exception $exception) {
            $this->addFlash('error', $exception->getMessage());
        }

        return $this->redirectToRoute('index', [], 302);
    }
}
