<?php

namespace App\Factory;

use App\Entity\Race;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class RaceFactory
{
    const MAX_RACES_COUNT = 3;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var HorseFactory
     */
    private $horseFactory;

    /** @var int */
    private $numberOfHorses;

    public function __construct(
        EntityManagerInterface $entityManager,
        HorseFactory $horseFactory,
        int $numberOfHorses
    ) {
        $this->entityManager = $entityManager;
        $this->horseFactory = $horseFactory;
        $this->numberOfHorses = $numberOfHorses;
    }

    public function createRace(): Race
    {


        $race = new Race();

        for ($i = 0; $i < $this->numberOfHorses; $i ++) {
            $race->addHorse($this->horseFactory->create());
        }

        $this->entityManager->persist($race);
        $this->entityManager->flush($race);

        return $race;
    }
}
