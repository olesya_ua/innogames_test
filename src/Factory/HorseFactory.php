<?php

namespace App\Factory;

use App\Entity\Horse;

class HorseFactory
{
    /** @var float */
    private $horseMinStat;

    /** @var float */
    private $horseMaxStat;

    public function __construct(float $horseMinStat, float $horseMaxStat)
    {
        $this->horseMinStat = $horseMinStat;
        $this->horseMaxStat = $horseMaxStat;
    }

    public function create(): Horse
    {
        return new Horse(
            $this->generateRandomStat(),
            $this->generateRandomStat(),
            $this->generateRandomStat()
        );
    }

    private function generateRandomStat(): float
    {
        return $this->floadRand($this->horseMinStat, $this->horseMaxStat, 1);
    }

    private function floadRand(float $min, float $max, int $decimals): float
    {
        $scale = pow(10, $decimals);

        return mt_rand($min * $scale, $max * $scale) / $scale;
    }
}
