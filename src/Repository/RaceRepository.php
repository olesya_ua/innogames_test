<?php

namespace App\Repository;

use App\Entity\Race;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Race|null find($id, $lockMode = null, $lockVersion = null)
 * @method Race|null findOneBy(array $criteria, array $orderBy = null)
 * @method Race[]    findAll()
 * @method Race[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Race::class);
    }

    public function getCurrentRaces()
    {
        $currentRacesCriteria = new Criteria();
        $currentRacesCriteria->where($currentRacesCriteria->expr()->eq('finishedAt', null))
            ->orderBy(['startedAt' => $currentRacesCriteria::DESC]);

        return $this->matching($currentRacesCriteria);
    }

    public function getLastRaces($limit = 5)
    {
        $lastRacesCriteria = new Criteria();
        $lastRacesCriteria->where($lastRacesCriteria->expr()->neq('finishedAt', null))
            ->setMaxResults($limit)
            ->orderBy(['finishedAt' => $lastRacesCriteria::DESC]);

        return $this->matching($lastRacesCriteria);
    }
}
