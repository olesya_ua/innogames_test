<?php

namespace App\Repository;

use App\Entity\Horse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Horse|null find($id, $lockMode = null, $lockVersion = null)
 * @method Horse|null findOneBy(array $criteria, array $orderBy = null)
 * @method Horse[]    findAll()
 * @method Horse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HorseRepository extends ServiceEntityRepository
{
    private $raceDistance;

    public function __construct(ManagerRegistry $registry, int $raceDistance)
    {
        parent::__construct($registry, Horse::class);

        $this->raceDistance = $raceDistance;
    }

    public function findBestHorse(): ?Horse
    {
        $horseCriteria = new Criteria();
        $horseCriteria->where($horseCriteria->expr()->eq('currentDistance', $this->raceDistance))
            ->setMaxResults(1)
            ->orderBy(['finishedInSeconds' => $horseCriteria::ASC]);
        $horses = $this->matching($horseCriteria);

        return $horses->first() ?: null;
    }
}
