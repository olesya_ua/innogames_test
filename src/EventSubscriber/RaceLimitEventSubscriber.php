<?php

namespace App\EventSubscriber;

use App\Entity\Race;
use App\Repository\RaceRepository;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class RaceLimitEventSubscriber implements EventSubscriber
{
    /**
     * @var int
     */
    private $raceLimit;

    public function __construct(int $raceLimit)
    {
        $this->raceLimit = $raceLimit;
    }

    public function getSubscribedEvents(): array
    {
        return [Events::prePersist];
    }

    public function prePersist(LifecycleEventArgs $event)
    {
        $object = $event->getObject();
        if ($object instanceof Race) {
            /** @var RaceRepository $repository */
            $repository = $event->getEntityManager()->getRepository(Race::class);
            $races = $repository->findBy(
                ['finishedAt' => null]
            );

            if (count($races) >= $this->raceLimit) {
                throw new \DomainException(
                    sprintf('There are can be only %d races at the same time', $this->raceLimit)
                );
            }
        }
    }
}
