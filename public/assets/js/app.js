var initHorseTooltips = function () {
    setTimeout(function () {
        $('.progress .point i').popover({
            container: 'body'
        });
        $('.progress .point i').popover('show')
    }, 1000);
};

$(document).ready(initHorseTooltips);
