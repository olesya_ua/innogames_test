# Horse races
## How to set up this project
1. Install composer dependencies with CLI command `composer install`
2. Open `.env` and set `DATABASE_URL` environment variable value
3. Create database with CLI command `bin/console doctrine:database:create`
4. Create database scheme with `bin/console doctrine:migration:migrate`
5. Run a local web server with document root pointing to `public` directory
